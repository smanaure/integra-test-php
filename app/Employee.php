<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Employee extends Model
{
    public function getHiringDateAttribute($value){
        $date = Carbon::createFromFormat('Y-m-d', $value);
        return $date->format('m/d/Y');
    }
}
