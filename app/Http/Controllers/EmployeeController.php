<?php

namespace App\Http\Controllers;

use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($search = null)
    {
        // Obtener lista de empleados, si se tiene un parametro de busqueda
        // se compara dicho parametro con el apellido y el email
        $employees = Employee::when($search !== null, function($q) use($search){
            return $q->where('last_name', 'like', '%'.$search.'%')->orWhere('email', 'like', '%'.$search.'%');
        })->orderBy('id')->get();
        return response()->json(['search' => $search, 'employees' => $employees]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Crear nuevo empleado en base de datos
        $employee = new Employee();
        $employee->last_name = $request->last_name;
        $employee->first_name = $request->first_name;
        $employee->phone = $request->phone;
        $employee->email = $request->email;
        // Formatear input de fecha para base de datos
        $employee->hiring_date = Carbon::createFromFormat('m/d/Y',$request->hiring_date);
        $employee->save();
        return redirect('/employees/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        // Se utiliza la vista de edición deshabilitada para mostrar los datos del empleado
        $edit = false;
        return view('employees.edit', compact('employee', 'edit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $edit = true;
        return view('employees.edit', compact('employee', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        // Se actualizan los datos del empleado obtenido por parametro
        $employee->last_name = $request->last_name;
        $employee->first_name = $request->first_name;
        $employee->phone = $request->phone;
        $employee->email = $request->email;
        $employee->hiring_date = Carbon::createFromFormat('m/d/Y',$request->hiring_date);
        $employee->save();
        return redirect('/employees/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        // Eliminar empleado y redireccionar al listado
        $employee->delete();
        return redirect('/employees/list');
    }
}
