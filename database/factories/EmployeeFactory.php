<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'last_name' => $faker->lastName(),
        'first_name' => $faker->firstName(),
        'phone' => '(888) 937-7238',
        'email' => $faker->unique()->safeEmail,
        'hiring_date' => now(),
    ];
});