## Descripción

Prueba técnica de conocimientos PHP

**PARTE I**

Formulario web para la entrada / salida de datos conexión a base de datos MySQL

- ID de empleado debe ser único
- Apellido del empleado
- Nombre del empleado
- Teléfono formateado (XXX) XXX-XXXX
- Correo del empleado
- Fecha de contratación formateada como MM / DD / AAAA

**Puntos Adicionales**
- Pantalla de búsqueda donde puede ingresar el apellido del empleado o el correo del empleado y devuelve todos los datos de la búsqueda.
- El formulario debe ser capaz de crear, modificar, eliminar y consultar los empleados.


**PARTE II**

Consumir [este API](https://api.coindesk.com/v1/bpi/currentprice.json) y mostrar datos en una gráfica que se actualice cada 30 segundos, con una hora de historial.

## Requerimientos
- PHP Version 7.3.11
- MariaDB 10.4.8
- Composer 2.0.12
- Conexión a internet

## Instalación
1. Crear una base de datos **vacía**.
2. Clonar este repositorio.
3. Abrir la terminar en la raíz del proyecto y ejecutar `composer install`.
4. Copiar el archivo *.env.example* en *.env*.
5. Ingresar en *.env* los datos de conexión a base de datos, especificamente:

        DB_CONNECTION
        DB_HOST
        DB_PORT
        DB_DATABASE
        DB_USERNAME
        DB_PASSWORD

6. En la terminal dentro de la raíz del proyecto ejecutar `php artisan migrate:fresh --seed`
7. En la terminal dentro de la raíz del proyecto ejecutar `php artisan serve`
8. Ingresar desde un navegador a `http://localhost:8000`

Autor [Sócrates Manaure](https://www.linkedin.com/in/socratesmanaurelandaeta).