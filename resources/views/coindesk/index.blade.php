@extends('layouts.main')
@section('content')
    <div>
        <div class="row">
            <div class="col-8">
                <h2>CoinDesk</h2>
            </div>
            <div class="col-4">
                <p class="float-right">Ultima consulta: <b><span id="lastUpdatedAt"></span></b></p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <canvas id="coinDeskChart" width="400" height="150"></canvas>
            </div>
        </div>
    </div>
@stop

@section('scripts')
<script>
    $( document ).ready(function() {
        const ctx = $('#coinDeskChart');
        // Opciones iniciales de Chartjs
        let chartData = {
            labels: [],
            datasets: [
                {
                    label: "USD",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(0,122,12,0.4)",
                    borderColor: "rgba(0,122,12,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(0,122,12,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(0,122,12,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: [],
                },
                {
                    label: "GBP",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(122,0,65,0.4)",
                    borderColor: "rgba(122,0,65,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(122,0,65,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(122,0,65,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: [],
                },
                {
                    label: "EUR",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(0,8,122,0.4)",
                    borderColor: "rgba(0,8,122,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(0,8,122,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(0,8,122,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 5,
                    pointHitRadius: 10,
                    data: [],
                },
            ]
        };
        // Instancia de Chartjs
        const coinDeskChart = new Chart(ctx, {
            type: 'line',
            data: chartData,
            options: {
                showLines: true
            }
        });

        // Función para obtener data y actualizar gráfica
        function addData(){
            // Llamado al API de CoinDesk
            $.ajax({
                url : "https://api.coindesk.com/v1/bpi/currentprice.json",
                type : 'GET',
                dataType:'json',
                success : function(data) {
                    // Obtengo la fecha actual del resultado de la consulta
                    const currentDate = new Date();
                    // Obtengo los diferentes segmentos de la fecha para armar el label
                    const year = currentDate.getFullYear();
                    const month = (currentDate.getMonth() + 1) > 9 ? `${(currentDate.getMonth() + 1)}` : `0${(currentDate.getMonth() + 1)}`;
                    const day = currentDate.getDate() > 9 ? `${currentDate.getDate()}` : `0${currentDate.getDate()}`;
                    const hours = currentDate.getHours() > 9 ? `${currentDate.getHours()}` : `0${currentDate.getHours()}`;
                    const minutes = currentDate.getMinutes() > 9 ? `${currentDate.getMinutes()}` : `0${currentDate.getMinutes()}`;
                    const seconds = currentDate.getSeconds() > 9 ? `${currentDate.getSeconds()}` : `0${currentDate.getSeconds()}`;
                    // Armo el label
                    const label = `${year}/${month}/${day} ${hours}:${minutes}:${seconds}`;
                    const labelChart = `${hours}:${minutes}:${seconds}`;
                    // Se verifica si ya tenemos la hora de historial, 1 hora cada 30 segundos son 120 registros
                    if(coinDeskChart.data.datasets[0].data.length >= 120){
                        // Elimino el primer valor de cada una de las lineas
                        coinDeskChart.data.datasets[0].data.shift();
                        coinDeskChart.data.datasets[1].data.shift();
                        coinDeskChart.data.datasets[2].data.shift();
                        coinDeskChart.data.labels.shift();
                    }
                    // Agrego el valor de USD
                    coinDeskChart.data.datasets[0].data.push(data.bpi.USD.rate_float);
                    // Agrego el valor de GBP
                    coinDeskChart.data.datasets[1].data.push(data.bpi.GBP.rate_float);
                    // Agrego el valor de EUR
                    coinDeskChart.data.datasets[2].data.push(data.bpi.EUR.rate_float);
                    // Agrego el nuevo label
                    coinDeskChart.data.labels.push(`${labelChart}`);
                    // Actualizo el indicador de ultima actualización
                    $("#lastUpdatedAt").html(label);
                    // Actualizo la gráfica
                    coinDeskChart.update();
                },
                error : function(request,error){
                    console.log(`Ocurrió un error...`, error);
                }
            });
        }
        // llamada a la función de la gráfica al iniciar la página
        addData();

        // Intervalo de llamada a la gráfica, 30 segundos
        setInterval(() => {
            addData();
        }, 30000);
    });
</script>
@stop