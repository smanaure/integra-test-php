@extends('layouts.main')
@section('content')
    <div>
        <div class="row">
            @if($edit)
                <div class="col-10">
                    <h2>Edición de Empleado: {{$employee->last_name}} {{$employee->first_name}}</h2>
                </div>
            @else
                <div class="col-10">
                    <h2>Datos de: {{$employee->last_name}} {{$employee->first_name}}</h2>
                </div>
            @endif
        </div>
        <div class="row">
            <div class="col">
                <form method="POST" action="{{url('employees')}}/{{$employee->id}}" id="createForm">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="last_name">Apellido</label>
                        <input @if(!$edit) disabled @endif placeholder="Introduzca el Apellido" type="text" class="form-control" value="{{$employee->last_name}}" id="last_name" name="last_name">
                    </div>
                    <div class="form-group">
                        <label for="first_name">Nombre</label>
                        <input @if(!$edit) disabled @endif placeholder="Introduzca el Nombre" type="text" class="form-control" value="{{$employee->first_name}}" id="first_name" name="first_name">
                    </div>
                    <div class="form-group">
                        <label for="phone">Teléfono</label>
                        <input @if(!$edit) disabled @endif placeholder="Introduzca el Teléfono" type="text" class="form-control" value="{{$employee->phone}}" id="phone" name="phone">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input @if(!$edit) disabled @endif placeholder="Introduzca el E-mail" type="email" class="form-control" value="{{$employee->email}}" id="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="hiring_date">Fecha de contratación</label>
                        <input @if(!$edit) disabled @endif placeholder="MM/DD/AAAA" autocomplete="off" type="text" class="form-control" value="{{$employee->hiring_date}}" id="hiring_date" name="hiring_date">
                    </div>
                    @if($edit)
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                        <p class="text-danger" id="errormsg" style="display: none;"></p>
                    @endif
                </form>
                <br>
            </div>
        </div>
    </div>
@stop

@section('scripts')
<script>
$( document ).ready(function() {
    // Función para formatear el número de teléfono
    function formatPhoneNumber(phoneNumberString) {
        let cleaned = ('' + phoneNumberString).replace(/\D/g, '');
        const part1 = cleaned.length > 2 ? `(${cleaned.substring(0,3)})` : cleaned
        const part2 = cleaned.length > 3 ? ` ${cleaned.substring(3, 6)}` : ''
        const part3 = cleaned.length > 6 ? `-${cleaned.substring(6, 10)}` : ''    
        return `${part1}${part2}${part3}`;
    }
    
    // Detecta el cambio del input de teléfono para formatearlo
    $("#phone").change(function(){
        $("#phone").val(formatPhoneNumber($("#phone").val()));
    });
    
    // inicializar el datepicker
    $('#hiring_date').datepicker({});

    // Validación del formulario, todos los campos son requeridos
    $("#createForm").submit(function(e){
        $("#errormsg").hide();
        if(!$("#last_name").val() || $("#last_name").val().lenght === 0){
            $("#errormsg").html('Debe introducir un apellido');
            $("#errormsg").show();
            e.preventDefault();
            return
        }
        if(!$("#first_name").val() || $("#first_name").val().lenght === 0){
            $("#errormsg").html('Debe introducir un nombre');
            $("#errormsg").show();
            e.preventDefault();
            return
        }
        if(!$("#phone").val() || $("#phone").val().lenght === 0){
            $("#errormsg").html('Debe introducir un teléfono');
            $("#errormsg").show();
            e.preventDefault();
            return
        }
        if(!$("#email").val() || $("#email").val().lenght === 0){
            $("#errormsg").html('Debe introducir un e-mail');
            $("#errormsg").show();
            e.preventDefault();
            return
        }
        if(!$("#hiring_date").val() || $("#hiring_date").val().lenght === 0){
            $("#errormsg").html('Debe introducir una fecha de contratación');
            $("#errormsg").show();
            e.preventDefault();
            return
        }
    })
});
</script>
@stop