@extends('layouts.main')
@section('content')
    <div>
        <div class="row">
            <div class="col-10">
                <h2>Empleados</h2>
            </div>
            <div class="col-2 float-right">
                <a href="{{url('/employees/create')}}" class="btn btn-primary float-right">Crear</a>
            </div>
        </div>
        @include('layouts.loader')
        <br>
        <div class="data row" style="display: none;">
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Buscar por apellido o email" id="searchInput" autocomplete="off">
            <div class="input-group-append">
                <span class="input-group-text" id="addon" style="cursor:pointer;">Buscar</span>
            </div>
        </div>
            <table class="table" id="dataTable">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">APELLIDO</th>
                        <th scope="col">NOMBRE</th>
                        <th scope="col">TELÉFONO</th>
                        <th scope="col">EMAIL</th>
                        <th scope="col">FECHA DE CONTRATACIÓN</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
<script>
// Alerta de confirmación para eliminar empleado
function deleteEmployee(id){
    Swal.fire({
        title: '¿Seguro desea eliminar este usuario?',
        showCancelButton: true,
        confirmButtonText: 'Si',
        cancelButtonText: 'Cancelar',
        confirmButtonColor: '#0062cc',
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.replace(`{{url('employees/delete')}}/${id}`);
        }
    })
}
$( document ).ready(function() {
    // Obtener el listado de empleados según un parametro
    function getData(param){
        // Mostrar loader
        $('.loader').show();
        // Ocultar tabla
        $('.data').hide();
        // Realizar consulta
        $.ajax({
            url : "{{url('employees/search')}}" + `/${param}`,
            type : 'GET',
            dataType:'json',
            success : function(data) {              
                // Ocultar loader
                $('.loader').hide();
                // Mostrar tabla
                $('.data').show();
                // Vaciar información previa en la tabla
                $("#dataTable > tbody").html("");
                let newHTML = ``;
                // Armar nueva información
                data.employees.forEach((emp) => {
                    newHTML += `<tr>`;
                        newHTML += `<td>${emp.id}</td>`;
                        newHTML += `<td>${emp.last_name}</td>`;
                        newHTML += `<td>${emp.first_name}</td>`;
                        newHTML += `<td>${emp.phone}</td>`;
                        newHTML += `<td>${emp.email}</td>`;
                        newHTML += `<td>${emp.hiring_date}</td>`;
                        newHTML += `<td>
                            <a class="btn btn-success" href="{{url('employees/show')}}/${emp.id}"> Ver </a>
                            <a class="btn btn-info" href="{{url('employees/edit')}}/${emp.id}"> Editar </a>
                            <button class="btn btn-danger btn-eliminar" onClick="deleteEmployee(${emp.id})"> Eliminar </button>
                        </td>`;
                    newHTML += `</tr>`;
                })
                // insertar nueva información
                $("#dataTable > tbody").html(newHTML);
            },
            error : function(request,error){
                // Ocultar loader
                $('.loader').hide();
                console.log(`Ocurrió un error...`, error);
            }
        });
    }
    // Ocultar tabla
    $('.data').hide();
    // Obtener información inicial, sin filtro
    getData('');
    // Buscar información al clickear buscar
    $("#addon").click(function(){
        getData($("#searchInput").val());
    });
});
</script>
@stop