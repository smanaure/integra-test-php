@extends('layouts.main')
@section('content')
<div class="jumbotron">
    <h1 class="display-4">iNTEGRA - TEST PHP</h1>
    <p class="lead">
      Prueba de conocimientos PHP realizada por <a href="https://www.linkedin.com/in/socratesmanaurelandaeta" target="_blank" rel="noopener noreferrer">Sócrates Manaure</a>
    </p>
</div>
@stop