<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/employees/list', function () {
    return view('employees.index');
});
Route::get('employees/search/{search?}', 'EmployeeController@index');
Route::get('/employees/create', function () {
    return view('employees.create');
});
Route::post('employees', 'EmployeeController@store');
Route::get('employees/edit/{employee}', 'EmployeeController@edit');
Route::get('employees/show/{employee}', 'EmployeeController@show');
Route::put('employees/{employee}', 'EmployeeController@update');
Route::get('employees/delete/{employee}', 'EmployeeController@destroy');

Route::get('/coindesk', function () {
    return view('coindesk.index');
});